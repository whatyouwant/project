package models

import "time"

type Product struct {
    ID              uint64      `gorm:"primary_key;auto_increment" json:"id"`
    Product_name    string      `gorm:"size:255;not null;unique" json:"product_name"`
    Quantity        uint32      `gorm:"not null" json:"quantity"`
    Cost            uint32      `gorm:"not null" json:"cost"`
    Image           string      `gorm:"not null" json:"image"`
    CreatedAt       time.Time   `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
    UpdatedAt       time.Time   `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}
