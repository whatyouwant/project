package controllers

import (
    "net/http"
    "encoding/json"

    "gitlab.com/whatyouwant/project/server/products/database"
)

func AddProduct(w http.ResponseWriter, r *http.Request) {
    w.Header().Set("Context-Type", "application/json")
    w.Header().Set("Context-Type", "application/x-www-form-urlencoded")
    w.Header().Set("Access-Control-Allow-Origin", "*")

    send := database.AddProduct(r)
    json.NewEncoder(w).Encode(send)
}
