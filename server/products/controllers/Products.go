package controllers

import (
    "encoding/json"
    "net/http"

    "gitlab.com/whatyouwant/project/server/products/database"
)

func Products(w http.ResponseWriter, r *http.Request) {
    w.Header().Set("Content-Type", "application/json")
    w.Header().Set("Context-Type", "application/x-www-form-urlencoded")
    w.Header().Set("Access-Control-Allow-Origin", "*")

    send := database.GetProducts()
    json.NewEncoder(w).Encode(send)
}
