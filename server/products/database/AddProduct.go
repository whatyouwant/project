package database

import (
    "net/http"
    "io/ioutil"
    "encoding/json"

    "gitlab.com/whatyouwant/project/server/products/models"

)

func AddProduct(r *http.Request) (*models.SendCreateProduct) {

    send := models.SendCreateProduct{}
    body, err := ioutil.ReadAll(r.Body)
    if err != nil {
        send.Success = "false"
        send.Token = ""
        send.Message = "Invalid form of data"
        send.Status = http.StatusUnprocessableEntity
        send.Payload = ""
        return &send
    }

    product := models.Product{}
    err = json.Unmarshal(body, &product)
    if err != nil {
        send.Success = "false"
        send.Token = ""
        send.Message = "Unmarshal is not happened"
        send.Status = http.StatusUnprocessableEntity
        send.Payload = ""
        return &send

    }

    check := Validate(product)
    if check != ""{
        send.Success = "false"
        send.Token = ""
        send.Message = check
        send.Status = http.StatusUnprocessableEntity
        send.Payload = ""
        return &send
    }

    err = db.Debug().Create(&product).Error
    if err != nil {
        send.Success = "false"
        send.Token = ""
        send.Message = "Product is already exists"
        send.Status = http.StatusUnprocessableEntity
        send.Payload = ""
        return &send
    }

    send.Success = "true"
    send.Token = ""
    send.Message = "Product is successfully added"
    send.Status = http.StatusCreated
    send.Payload = product.Product_name
    return &send
}

func Validate(product models.Product) (string) {
    if (product.Product_name == "" || product.Quantity < 0 || product.Cost < 0) {
        return ("Fields are empty or invalid entries")
    } else {
        return ""
    }
}
