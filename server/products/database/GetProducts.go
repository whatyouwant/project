package database

import (

    "net/http"

    "gitlab.com/whatyouwant/project/server/products/models"
)

func GetProducts() (*models.SendGetProducts) {
    var err error
    send := models.SendGetProducts{}
    products := []models.Product{}
    err = db.Debug().Model(&models.Product{}).Limit(100).Find(&products).Error


    if err != nil {
        send.Success = "false"
        send.Token  = ""
        send.Message = "Product data is not fetched"
        send.Status = http.StatusInternalServerError
        send.Payload = products
        return &send
    }
    send.Success = "true"
    send.Token = "some string"
    send.Message = "Products data is fetch"
    send.Status = http.StatusOK
    send.Payload = products

    return &send
}
