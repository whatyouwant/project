module gitlab.com/whatyouwant/project/server/products

go 1.13

require (
	github.com/gorilla/mux v1.7.4
	github.com/jinzhu/gorm v1.9.12
	github.com/joho/godotenv v1.3.0
	github.com/sirupsen/logrus v1.6.0
)
