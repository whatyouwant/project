package seed

import (

	"gitlab.com/whatyouwant/project/server/products/models"

	"github.com/jinzhu/gorm"
    Log "github.com/sirupsen/logrus"
)

var products = []models.Product{
    models.Product{
        Product_name:  "Product1",
        Image: "Image1 URL",
        Cost:800,
        Quantity:6,
    },
    models.Product{
        Product_name:  "Product2",
        Image: "Image2 URL",
        Cost:520,
        Quantity:3,
    },
}


func Load(db *gorm.DB) {

	err := db.Debug().DropTableIfExists(&models.Product{}).Error
	if err != nil {
		Log.Fatal("[SEEDER]: Cannot drop table: ", err)
	}
	err = db.Debug().AutoMigrate(&models.Product{}).Error
	if err != nil {
		Log.Fatal("[SEEDER]: Cannot migrate table: ", err)
	}

	for i, _ := range products {
		err = db.Debug().Model(&models.Product{}).Create(&products[i]).Error
		if err != nil {
			Log.Fatal("[SEEDER]: Cannot seed users table: ", err)
		}

	}
}
