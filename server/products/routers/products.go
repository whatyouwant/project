package routers

import (

    "gitlab.com/whatyouwant/project/server/products/controllers"
    "github.com/gorilla/mux"
)

func SetUserRouters(router *mux.Router) *mux.Router {
    router.HandleFunc("/api/products", controllers.Products).Methods("GET")
    router.HandleFunc("/api/addproduct", controllers.AddProduct).Methods("POST")
/*
    router.HandleFunc("/api/user/{id}", controllers.User).Methods("GET")
    router.HandleFunc("/api/user/{id}", controllers.Update).Methods("UPDATE")
    router.HandleFunc("/api/user/{id}", controllers.Delete).Methods("DELETE")
*/
    return router
}
