package envsetup

import (
    "os"

    "gitlab.com/whatyouwant/project/server/login/database"

    "github.com/joho/godotenv"
    Log "github.com/sirupsen/logrus"
)

var Server string

func LoadEnv() {
    var err error
    err = godotenv.Load()

    if err != nil {
        Log.Fatal("[ENVSETUP]: Env Variables are not set")
    } else {
        Log.Info("[ENVSETUP]: Loading Env Variables")
    }

    Server = os.Getenv("APP_PORT")

    database.Initialize(os.Getenv("DB_DRIVER"), os.Getenv("DB_USER"), os.Getenv("DB_PASSWORD"), os.Getenv("DB_PORT"), os.Getenv("DB_HOST"), os.Getenv("DB_NAME"))

}
