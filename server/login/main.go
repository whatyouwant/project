package main

import (

    "net/http"

    "gitlab.com/whatyouwant/project/server/login/envsetup"
    "gitlab.com/whatyouwant/project/server/login/routers"

    Log "github.com/sirupsen/logrus"
)

func main() {
    envsetup.LoadEnv()
    Log.Info("[MAIN]: Started LoadEnv...")

    router := routers.InitRoutes()
    Log.Info("[MAIN]: Started InitRoutes...")

    server := &http.Server {
        Addr: envsetup.Server,
        Handler: router,
    }
    Log.Info("[APP]: is listening at ", envsetup.Server)
    Log.Fatal(server.ListenAndServe())
}
