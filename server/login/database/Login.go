package database

import (
    "net/http"
    "io/ioutil"
    "encoding/json"

    "gitlab.com/whatyouwant/project/server/login/models"
    "gitlab.com/whatyouwant/project/server/login/password"
    "gitlab.com/whatyouwant/project/server/login/auth"

)

func Login(r *http.Request) (*models.SendLogin) {

    send := models.SendLogin{}
    userInfo := models.UserInfo{}
    body, err := ioutil.ReadAll(r.Body)
    if err != nil {
        send.Success = "false"
        send.Token = ""
        send.Message = "Invalid form of data"
        send.Status = http.StatusUnprocessableEntity
        send.Payload = ""
        return &send
    }

    err = json.Unmarshal(body, &userInfo)
    if err != nil {
        send.Success = "false"
        send.Token = ""
        send.Message = "Unmarshal is not happened"
        send.Status = http.StatusUnprocessableEntity
        send.Payload = ""
        return &send

    }

    check := Validate(userInfo)
    if check != ""{
        send.Success = "false"
        send.Token = ""
        send.Message = check
        send.Status = http.StatusUnprocessableEntity
        send.Payload = ""
        return &send
    }

    dbData := models.User{}

    err = db.Debug().Model(models.User{}).Where("email = ?", userInfo.Email).Take(&dbData).Error

    if err != nil {
        send.Success = "false"
        send.Token = ""
        send.Message = "Invalid User"
        send.Status = http.StatusBadRequest
        send.Payload = ""
        return &send
    }

    if !password.VerifyPassword(userInfo.Password, dbData.Password) {
        send.Success = "false"
        send.Token = ""
        send.Message = "Invalid User"
        send.Status = http.StatusBadRequest
        send.Payload = ""
        return &send

    }
    var Token string
    Token, err = auth.CreateToken(dbData.ID)

    if err != nil {
        send.Success = "false"
        send.Token = ""
        send.Message = "Token is not generated"
        send.Status = http.StatusInternalServerError
        send.Payload = ""
        return &send

    }

    loginInfo := models.Login{}
    loginInfo.ID = dbData.ID
    loginInfo.Token = Token

    err = db.Debug().Create(&loginInfo).Error

    send.Success = "true"
    send.Token = Token
    send.Message = "User is successfully logged in"
    send.Status = http.StatusOK
    send.Payload = dbData.Name
    return &send
}

func Validate(user models.UserInfo) (string) {
    if (user.Email == ""  || user.Password == "") {
        return ("Fields should not be empty")
    } else {
        return ""
    }
}
