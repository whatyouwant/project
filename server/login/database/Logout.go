package database

import (
    "net/http"

    "gitlab.com/whatyouwant/project/server/login/models"
    "gitlab.com/whatyouwant/project/server/login/auth"
)

type LogoutReq struct {
    Token   string  `json:"token"`
}
func Logout(r *http.Request) (*models.SendLogin) {
    send := models.SendLogin{}
/*    logReq := LogoutReq{}
    body, err := ioutil.ReadAll(r.Body)
    if err != nil {
        send.Success = "false"
        send.Token = ""
        send.Message = "Invalid form of data"
        send.Status = http.StatusUnprocessableEntity
        send.Payload = ""
        return &send
    }

    err = json.Unmarshal(body, &logReq)

    if err != nil {
        send.Success = "false"
        send.Token = logReq.Token
        send.Message = "Unmarshal is not happened"
        send.Status = http.StatusUnprocessableEntity
        send.Payload = ""
        return &send

    }*/
    Token := auth.ExtractToken(r)
    Uid, err := auth.ExtractTokenID(r)
    dbData := models.Login{}

    del := db.Debug().Model(&models.Login{}).Where("id = ? and token = ?", Uid, Token).Take(&dbData).Delete(models.Login{})


    if del.Error != nil || err != nil{
        send.Success = "false"
        send.Token = Token
        send.Message = "Invalid Token"
        send.Status = http.StatusInternalServerError
        send.Payload = ""
        return &send

    }

    err = db.Model(&models.User{}).Update("last_logged_in", dbData.LoginTime).Error

    if err != nil {
        send.Success = "false"
        send.Token = Token
        send.Message = "User is not logged out"
        send.Status = http.StatusInternalServerError
        send.Payload = ""
        return &send
    }

    send.Success = "true"
    send.Token = ""
    send.Message = "User is successfully logged out"
    send.Status = http.StatusOK
    send.Payload = ""
    return &send
}
