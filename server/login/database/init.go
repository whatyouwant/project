package database

import (
    "fmt"

    "gitlab.com/whatyouwant/project/server/login/models"

    "github.com/jinzhu/gorm"
    _ "github.com/jinzhu/gorm/dialects/mysql"
    Log "github.com/sirupsen/logrus"
)

var db *gorm.DB

func Initialize(DBDriver, DBUser, DBPassword, DBPort, DBHost, DBName string) {
    var err error
    if (DBDriver == "mysql") {

        DBURL := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local", DBUser, DBPassword, DBHost, DBPort, DBName)

        db, err = gorm.Open(DBDriver, DBURL)
        if err != nil {
            Log.Warn("[DATABASE]: Cannot connect to ",DBDriver)
            Log.Fatal("[DATABASE]: ",err)
        } else {
            Log.Info("[DATABASE]: Connection Established")
        }
    }
    db.Debug().AutoMigrate(&models.Login{})
    err = db.Debug().Model(&models.Login{}).AddForeignKey("id", "users(id)", "cascade", "cascade").Error
    if err != nil {
        Log.Fatal("attaching foreign key error: %v", err)
    }


}
