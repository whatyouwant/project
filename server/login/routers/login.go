package routers

import (

    "gitlab.com/whatyouwant/project/server/login/controllers"
    "github.com/gorilla/mux"
)

func SetUserRouters(router *mux.Router) *mux.Router {
    router.HandleFunc("/api/login", controllers.Login).Methods("POST")
    router.HandleFunc("/api/logout", controllers.Logout).Methods("POST")
    return router
}
