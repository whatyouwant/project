package models

type UserInfo struct {
    Email       string  `json:"email"`
    Password    string  `json:"password"`
}
