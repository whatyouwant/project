package models

import "time"

type Login struct {
    ID          uint32       `gorm "size:512; not null"`
    Token       string      `gorm:"size:512; not null; unique" json:"token"`
    LoginTime   time.Time   `gorm:"default:CURRENT_TIMESTAMP" json:"loggedin"`
}
