package seed

import (

	"gitlab.com/whatyouwant/project/server/users/models"

	"github.com/jinzhu/gorm"
    Log "github.com/sirupsen/logrus"
)

var users = []models.User{
	models.User{
		Name:       "ABC XYZ",
		Email:      "abc@gmail.com",
		Password:   "abc",
	},
	models.User{
		Name:       "PQR JKL",
		Email:      "jkl@gmail.com",
		Password:   "pql",
	},
}

func Load(db *gorm.DB) {

	err := db.Debug().DropTableIfExists(&models.User{}).Error
	if err != nil {
		Log.Fatal("[SEEDER]: Cannot drop table: ", err)
	}
	err = db.Debug().AutoMigrate(&models.User{}).Error
	if err != nil {
		Log.Fatal("[SEEDER]: Cannot migrate table: ", err)
	}

	for i, _ := range users {
		err = db.Debug().Model(&models.User{}).Create(&users[i]).Error
		if err != nil {
			Log.Fatal("[SEEDER]: Cannot seed users table: ", err)
		}

	}
}
