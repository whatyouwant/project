package routers

import (

    "gitlab.com/whatyouwant/project/server/users/controllers"
    "github.com/gorilla/mux"
)

func SetUserRouters(router *mux.Router) *mux.Router {
    router.HandleFunc("/api/users", controllers.Users).Methods("GET")
    router.HandleFunc("/api/register", controllers.Create).Methods("POST")
/*
    router.HandleFunc("/api/user/{id}", controllers.User).Methods("GET")
    router.HandleFunc("/api/user/{id}", controllers.Update).Methods("UPDATE")
    router.HandleFunc("/api/user/{id}", controllers.Delete).Methods("DELETE")
*/
    return router
}
