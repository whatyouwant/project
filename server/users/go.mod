module gitlab.com/whatyouwant/project/server/users

go 1.13

require (
	github.com/badoux/checkmail v0.0.0-20181210160741-9661bd69e9ad
	github.com/gorilla/mux v1.7.4
	github.com/jinzhu/gorm v1.9.12
	github.com/joho/godotenv v1.3.0
	github.com/sirupsen/logrus v1.6.0
	golang.org/x/crypto v0.0.0-20191205180655-e7c4368fe9dd
)
