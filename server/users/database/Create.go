package database

import (
    "net/http"
    "io/ioutil"
    "encoding/json"

    "gitlab.com/whatyouwant/project/server/users/models"
    "gitlab.com/whatyouwant/project/server/users/password"

    "github.com/badoux/checkmail"
)

func Create(r *http.Request) (*models.SendCreateUser) {

    send := models.SendCreateUser{}
    body, err := ioutil.ReadAll(r.Body)
    if err != nil {
        send.Success = "false"
        send.Token = ""
        send.Message = "Invalid form of data"
        send.Status = http.StatusUnprocessableEntity
        send.Payload = ""
        return &send
    }

    user := models.User{}
    err = json.Unmarshal(body, &user)
    if err != nil {
        send.Success = "false"
        send.Token = ""
        send.Message = "Unmarshal is not happened"
        send.Status = http.StatusUnprocessableEntity
        send.Payload = ""
        return &send

    }

    check := Validate(user)
    if check != ""{
        send.Success = "false"
        send.Token = ""
        send.Message = check
        send.Status = http.StatusUnprocessableEntity
        send.Payload = ""
        return &send
    }

    var pass string
    pass, err = password.HashPassword(user.Password)
    if err != nil {
        send.Success = "false"
        send.Token = ""
        send.Message = "Password is not hashed"
        send.Status = http.StatusInternalServerError
        send.Payload = ""
        return &send
    }
    user.Password = pass

    err = db.Debug().Create(&user).Error
    if err != nil {
        send.Success = "false"
        send.Token = ""
        send.Message = "Username is already exists"
        send.Status = http.StatusUnprocessableEntity
        send.Payload = ""
        return &send
    }

    send.Success = "true"
    send.Token = ""
    send.Message = "User is successfully created"
    send.Status = http.StatusCreated
    send.Payload = user.Name
    return &send
}

func Validate(user models.User) (string) {
    if (user.Email == "" || user.Name == "" || user.Password == "") {
        return ("Fields should not be empty")
    }
    if err := checkmail.ValidateFormat(user.Email); err != nil {
        return "Invalid Email"
    } else {
        return ""
    }
}
