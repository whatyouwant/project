package database

import (

    "net/http"

    "gitlab.com/whatyouwant/project/server/users/models"
)

func GetUsers() (*models.SendGetUsers) {
    var err error
    send := models.SendGetUsers{}
    users := []models.User{}
    err = db.Debug().Model(&models.User{}).Limit(100).Find(&users).Error


    if err != nil {
        send.Success = "false"
        send.Token  = ""
        send.Message = "User data is not fetched"
        send.Status = http.StatusInternalServerError
        send.Payload = users
        return &send
    }
    send.Success = "true"
    send.Token = "some string"
    send.Message = "Users data is fetch"
    send.Status = http.StatusOK
    send.Payload = users

    return &send
}
