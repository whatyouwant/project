package models

type SendGetUsers struct {
    Success string  `json:"success"`
    Token   string  `json:"token"`
    Message string  `json:"message"`
    Status  uint32  `json:"status_code"`
    Payload []User    `json:"payload"`
}
